var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('search', { title: 'Search Nearby Locations', locations: {} });
});
router.post('/find', function (req, res, next) {
  var collection = req.collection;
  var long = Number(req.body.longitude);
  var lat = Number(req.body.latitude);
  var lname = req.body.locationname;
  var cat = req.body.category;
  //query with $text and $geoWithin
  var geoquery = {
    'location': { '$geoWithin': { '$center': [[long, lat], 0.1] } },
    '$text': { '$search': lname + " " + cat }
  };
  //query with just fields matching and $near.
  var query = { 'name': lname, 'category': cat, 'location': { '$near': [long, lat] } };
  collection.find(geoquery).limit(3).toArray(function (err, docsarray) {
    if (err) throw err;
    res.render('result', { title: 'Result', locations: docsarray });
  });
});
module.exports = router;