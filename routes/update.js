var express = require('express');
var router = express.Router();
var ObjectId=require('mongodb').ObjectID;
/* GET home page. */
router.get('/', function (req, res, next) {
    var collection = req.collection;
    var locid = req.query.id;
    var query = { _id:new ObjectId(locid)};
   collection.find(query).toArray(function (err, doc) {
        if (err) throw err;
        res.render('update', { title: 'Update Location', location:doc});
    });
});
router.post('/edit', function (req, res, next) {
    var collection = req.collection;
    var id=req.query.id;
    var query={ _id:new ObjectId(id)};
    var operator={'$set':{'name':req.body.name,'category':req.body.category,'long':req.body.long,'lat':req.body.lat}};
    collection.update(query,operator, function (err, updatedLoc) {
        if(err) throw err;
        res.redirect('detail');
    });
    //  res.end(id);
});
module.exports = router;
