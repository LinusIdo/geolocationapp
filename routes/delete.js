var express = require('express');
var router = express.Router();
var ObjectId=require('mongodb').ObjectID;

/* GET home page. */
router.get('/', function(req, res, next) {
   var collection = req.collection;
    var locid = req.query.id;
    var query = { _id:new ObjectId(locid)};
   collection.find(query).toArray(function (err, doc) {
        if (err) throw err;
        res.render('delete', { title: 'Delete Location',msg:'Are you sure to delete this data?', location:doc});
    });
});
router.get('/remove',function(req,res,next){
    var collection = req.collection;
    var locid = req.query.id;
    var query = { _id:new ObjectId(locid)};
    collection.remove(query);
    res.redirect('detail');
    // res.end("deleted");
});
module.exports = router;
