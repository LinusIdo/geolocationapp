//Books collection
{
    '_id':"",
    'Name':"",
    'ISBN':"",
    'Author':["",""],
    'Keywords':["",""],
    'Students':[{
                  '_id':"",
                  'name':"",
                  'entry':"",
                  'email':""
                },
                {
                  '_id':"",
                  'name':"",
                  'entry':"",
                  'email':""
                }],
    'IssueDate':"",
    'ReturnDate':"",
    'IssuedBy':"",
}

//suggested indexes are 
db.Books.createIndex({'Keywords':1});
db.Books.createIndex({'ISBN':1,'Author':1});