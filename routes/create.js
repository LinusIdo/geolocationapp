var express = require('express');
var router = express.Router();
/* GET home page. */
router.get('/', function (req, res, next) {
    res.render('create', { title: 'Create New Location' });
});
router.post('/save', function (req, res,next) {
    req.assert('name', 'Full Name is required.').notEmpty();
    req.assert('category', 'Category is required.').notEmpty();
    req.assert('long', 'Longitude is required.').notEmpty();
    req.assert('lat', 'Latitude is required.').notEmpty();
    var errors = req.validationErrors();
    if (errors) res.render('contactus', { title: 'Contactus', error: errors });
    else {
        var collection = req.collection;
        var doc={name:req.body.name,category:req.body.category,long:req.body.long,lat:req.body.lat,location:[Number(req.body.long),Number(req.body.lat)]};
        collection.insert(doc,function(err,docInserted){
            if(err) throw err;           
           res.redirect('detail');
        });        
    }
});
module.exports = router;
