var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
    var collection = req.collection;
    collection.find().toArray(function(err,docsarray){
        if(err) throw err;
        res.render('detail', { title: 'Location List',points: docsarray});
    });    
});
module.exports = router;
