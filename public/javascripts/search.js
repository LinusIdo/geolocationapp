"use strict"
$(document).ready(function () {
    function success(position) {
        var crd = position.coords;
        var currentLon = crd.longitude;
        var currentLat = crd.latitude;
        document.getElementById("longitude").value = currentLon;
        document.getElementById("latitude").value = currentLat;
    }
    function fail(msg) {
        console.log(msg.code + msg.message); // Log the error
    }
    navigator.geolocation.getCurrentPosition(success, fail);
});